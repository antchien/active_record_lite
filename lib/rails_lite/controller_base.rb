require 'erb'
require_relative 'params'
require_relative 'session'
require 'active_support/core_ext'
require 'debugger'
require 'json'

class ControllerBase
  attr_reader :params

  def initialize(req, res, route_params={})
    @req = req
    @res = res
    @params = Params.new(req, route_params)

  end

  def session
    @session ||= Session.new(@req)
  end

  def already_rendered?
  end

  def redirect_to(url)
    @res.status = 302
    @res['Location'] = url
    session.store_session(@res)

    @already_built_response = true

  end

  def render_content(content, type)
    @res['Content-Type'] = type
    @res.body = content
    session.store_session(@res)

    @already_built_response = true

  end

  def render(template_name)
    template_path = "views/#{self.class.to_s.underscore}/#{template_name}.html.erb"
    erb = ERB.new(File.read(template_path))
    content = erb.result(self.get_binding)

    render_content(content, 'text/html')
  end

  def get_binding
    return binding()
  end

  def invoke_action(name)
  end
end
