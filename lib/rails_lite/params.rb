require 'uri'

class Params
  def initialize(req, route_params)
    @params = Hash.new(Array.new())
    query = req.query_string
    parse_www_encoded_form(query)
    parse_www_encoded_form(req.body)

  end

  def [](key)
  end

  def to_s
    @params.to_json.to_s
  end

  private
  def parse_www_encoded_form(www_encoded_form)
    unless www_encoded_form.nil?
      params_arr = URI.decode_www_form(www_encoded_form)
      params_arr.each do |param|
        key_nest = parse_key(param[0])
        if key_nest.size < 2
          @params[param[0]] = param[1]
        else
          temp = param[1]
          (0...key_nest.size-1).each do |index|
            new_hash = Hash.new
            new_hash[key_nest[key_nest.size-index-1]] = temp
            temp = new_hash
          end
          debugger
          @params[key_nest.first] = temp
        end
      end
    end
  end


  def parse_key(key)
    key_arr = key.split(/\]\[|\[|\]/)
  end
end
