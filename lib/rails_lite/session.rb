require 'json'
require 'webrick'

class Session
  def initialize(req)
    @req = req.cookies.select { |cookie| cookie.name == '_rails_lite_app' }
    if @req.first.value
      @cookie = JSON.parse(@req.first.value)
    else
      {}
    end
  end

  def [](key)
    @cookie[key]
  end

  def []=(key, val)
    @cookie[key] = val
  end

  def store_session(res)

    output_cookie = WEBrick::Cookie.new('_rails_lite_app', @cookie.to_json)
    res.cookies << output_cookie
  end
end
