require 'webrick'

root = File.expand_path '/'

server = WEBrick::HTTPServer.new :Port => 8080

server.start

server.mount_proc '/' do |req, res|
  res['Content-Type'] = 'text/text'
  res.body = req.path
end